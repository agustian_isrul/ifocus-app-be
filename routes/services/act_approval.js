const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();
const apiserver = conf.kktserver;
const fs = require('fs');
const ftp = require("basic-ftp");
router.get("/getAllapp", async (req, res, next) => {
    const token = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var token =req.tokenID
    var usersItems = [];
    var recordResult = [];
    // console.log('TOKEN DARI DEPAN :>> ', token);
    // token dcodeInfo.apps[0];
    var idtenant = dcodeInfo.idtenant;
    try {
        // filename, iduser, created_by, created_at, updated_at, active, idapprovval, id
       await axios
          .get(
            apiserver + "/adm/umanager/retriveusersadmin",
            {
              headers: { Authorization: token },
            }
          )
          .then(async (rsltusers) => {
            // console.log("Data Users : ", rsltusers.data);
            usersItems = rsltusers.data.data
          })
          .catch((err) => {
            console.log(err);
            return;
          });
      const appAdmins = await conn
        .select(
            "id", 
            "filename", 
            "created_at", 
            "active", 
            "created_by"
        )
        .from("t_payment")
        .orderBy("created_at", "desc");      
      if (appAdmins.length > 0) {
        // Get data from krakatoa //
        recordResult = [];
        await appAdmins.forEach(async (item) => {
            

            let fullobj = usersItems.find(o => o.id === item.created_by);
            // console.log("Fullname ", fullobj.fullname);
            if(fullobj) {
                let fullname = {"fullname" : fullobj.fullname};
                item = {...item, ...fullname};
            }
            // console.log("item ### ", item);

            recordResult.push(item);
            // var fullname = await lodash.filter(usersItems, x => x.id === item.id);
            // console.log("Fullname ###  ",fullname);
        })
        console.log("Kirim keluar #########################");

        // setTimeout(function () {
          res.status(200).json({ status: 200, data: recordResult });
        // }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: appAdmins });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  });
  router.get("/getapp/:id", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    try {
      let idapp = req.params.id;
      // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
      //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
  
      // var query = "SELECT  bank_code id, bank_code, app_code, bank_name, last_update_date, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, idtenant FROM m_app WHERE bank_code=$1 order by created_date desc";
      // const appAdmins = await pool.query(
      //   query, [bankcode]);
  
      const appAdmins = await conn
        .select(
          "id", 
            "appount", 
            "appname", 
            "bankname", 
            "balance", 
            "created_by", 
            "created_at", 
            "updated_at", 
            "active", 
            "idapproval"
        )
        .from("rekeningbank")
        .where("id", idapp)
      if (appAdmins.length > 0) {
        setTimeout(function () {
          res
            .status(200)
            .json({ status: 200, data: { appAdmins: appAdmins[0] } });
        }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  });

  
  router.post("/updateapp", async (req, res, next) => {
    var dcodeInfo = req.userData;
    try {
      // var apps = dcodeInfo.apps[0];
      const { id, appname, appount, bankname, balance } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      // payload = {
      //   id: this.appId,
      //   appname: this.groupForm.get('appname')?.value,
      //   appount: this.groupForm.get('appount')?.value,
      //   bankname: this.groupForm.get('bankname')?.value,
      //   balance: this.groupForm.get('balance')?.value,
      // };
      // const resp = await pool.query(
      //   "UPDATE m_app SET bank_code=$1, app_code=$2, bank_name=$3, last_update_date=$4 WHERE app_code=$5 ",
      //   [
      //     bank_code, app_code, bank_name, today, old_code
      //   ]
      // );
      const resp = await conn("rekeningbank").where("id", id).update({
        appount: appount,
        appname: appname,
        bankname: bankname,
        balance: balance,
        updated_at : today
      });
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_app ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  router.post("/updateappapprove", async (req, res, next) => {
    var dcodeInfo = req.userData;
    try {
      const { id, oldactive, isactive, idapproval } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
    
      const appAdmins = await conn.select(
            "id", 
            "filename", 
            "created_at", 
            "active", 
            "created_by"
        )
        .from("t_payment").where("id", id);
          

        let filePath = "./public/fileupload/"+appAdmins[0].filename;
        console.log("DATANYAAAAAAAAAAAAAAAAAAAAAAAAA ",filePath);

        if (fs.existsSync(filePath)) {
            //file exists
            console.log("Filenya ada");
            const client = new ftp.Client()
            client.ftp.verbose = true
            await client.access({
                host: "182.169.41.157",
                user: "bniact",
                password: "bniact",
                secure: false
            })
            console.log(await client.list());
            await client.uploadFrom(filePath, "/ftp/final/"+appAdmins[0].filename)
            console.log("Beres kirimnya");
            const resp = await conn("t_payment").where("id", id).update({
                    active:isactive,
                    updated_at:today
                  });
            if (resp > 0) {
                res.status(200).json({ status: 200, data: resp });
            } else {
            res.status(500).json({
                status: 500,
                data: "Error Update data ",
            });
            }




            // res.status(200).json({ status: 200, data: {} });

        } else {
            console.log("Ngga ada filenya");
        }





    //   const resp = await conn("t_payment").where("id", id).update({
    //     active:isactive,
    //     updated_at:today
    //   });
    //   if (resp > 0) {
    //     res.status(200).json({ status: 200, data: resp });
    //   } else {
    //     res.status(500).json({
    //       status: 500,
    //       data: "Error insert m_app ",
    //     });
    //   }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
 
  
  module.exports = router;