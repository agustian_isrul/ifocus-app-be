const pool = require("../../../connection/db");
const moment = require("moment");
insertLog = async (mactionfkid,
    description,
    locfkid,
    userfkid,
    refnum,
    routepath,
    status, tokencode) => {
    try {
      const resp = await pool.query("INSERT INTO public.trx_logger(mactionfkid, description, locfkid, userfkid, refnum, routepath, status, tokencode) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *", [
        mactionfkid,
        description,
        locfkid,
        userfkid,
        refnum,
        routepath,
        status,
        tokencode
      ]);
      if(resp.rowCount < 1) {
        return false;
      } else {
        return true;
      }
    } catch (err) {
      return false;
    }
  }

  insertLogContract = async (mactionfkid,
    description,
    locfkid,
    userfkid,
    refnum,
    routepath,
    status, tokencode, contractcode) => {
    try {
      const resp = await pool.query("INSERT INTO public.trx_logger(mactionfkid, description, locfkid, userfkid, refnum, routepath, status, tokencode, contractcode) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *", [
        mactionfkid,
        description,
        locfkid,
        userfkid,
        refnum,
        routepath,
        status,
        tokencode,
        contractcode
      ]);
      if(resp.rowCount < 1) {
        return false;
      } else {
        return true;
      }
    } catch (err) {
      return false;
    }
  }



  
  module.exports = { insertLog, insertLogContract }