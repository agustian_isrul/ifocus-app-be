const nodemailer = require("nodemailer");

async function sendemailtext(HOST, PORT, SendTo, SendFrom, Subject, Body, smtpUser, smtpPassword) {
    let transporter = nodemailer.createTransport({
        host: HOST,
        port: PORT,
        secure: true, // true for 465, false for other ports
        auth: {
          user: smtpUser, // generated ethereal user
          pass: smtpPassword, // generated ethereal password
        },
      });
      // send mail with defined transport object
        let info = await transporter.sendMail({
            from: SendFrom, // sender address
            to: SendTo, // list of receivers
            subject: Subject, // Subject line
            text: Body, // plain text body // html body
        });

    let result = {"code" : 1,"message" : "Mail sended"};
    return result;
}

async function sendmailattachment(HOST, PORT, SendTo, SendFrom, Subject, Body, smtpUser, smtpPassword, attachmentUrl, attachmentName) {
    let transporter = nodemailer.createTransport({
        host: HOST,
        port: PORT,
        secure: true, // true for 465, false for other ports
        auth: {
          user: smtpUser, // generated ethereal user
          pass: smtpPassword, // generated ethereal password
        },
      });
      // send mail with defined transport object
        let info = await transporter.sendMail({
            from: SendFrom, // sender address
            to: SendTo, // list of receivers
            subject: Subject, // Subject line
            text: Body, 
            attachments: [{filename: attachmentName, path:attachmentUrl }]// plain text body // html body
        });

    let result = {"code" : 1,"message" : "Mail sended"};
    return result;
}


function sendemailhtml(dataobject) {
    let result = {"code" : 1,"message" : "Mail sended"};
    return result;
}

module.exports = {
    sendemailtext,
    sendemailhtml,
    sendmailattachment
};