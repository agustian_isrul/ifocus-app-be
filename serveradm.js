const conf = require("./config.json");

const express = require("express"),
  app = express(),
  port = process.env.port || conf.port;
const cors = require("cors");
// var bodyParser = require('body-parser');
var notifRoute = require("./routes/services/serv_notification");
var operationunitRoute = require("./routes/services/serv_operationunit");
var supplierssRoute = require("./routes/services/serv_suppliers");

var path = require("path");

// const session = require("express-session");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
//-==================== PATH SETTING =================
//app.use(logger('dev'));
app.use(express.static(path.join(__dirname, "public")));
app.use("/spb/notification", notifRoute);
app.use("/spb/operationunit", operationunitRoute);
app.use("/spb/suppliers", supplierssRoute);
// app.use("/act/approval", approverRoute);


var server = app.listen(port, function () {
  let host = server.address().address;
  let portname = server.address().port;
  console.log("Example server is running in http://%s:%s", host, portname);
});
