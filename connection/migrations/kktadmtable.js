exports.up = function (knex) {
  return knex.schema
    .createTable("cdid_adminnotification", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("iduser").defaultTo(0);
      table.string("cd_busparam").notNullable();
      table.bigInteger("idtenant").defaultTo(0);
      table.bigInteger("idapp").defaultTo(0);
      table.bigInteger("created_byid").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_dbcorellation", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("description").notNullable();
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_liscenses", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table
        .string("licensetokens")
        .defaultTo("", (options = { constraintName: (string = undefined) }));
      table
        .text("licensejson")
        .defaultTo("", (options = { constraintName: (string = undefined) }));
      table.bigInteger("created_byid").defaultTo(0);
      table.timestamp("expiredate", { useTz: false }).notNullable();
      table.bigInteger("cdid_tenant").defaultTo(0);
      table.bigInteger("id_application").defaultTo(0);
      table.integer("paidstatus").defaultTo(0);
      table.integer("active").defaultTo(0);
      table.integer("defaultactive").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_orgapplications", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("idorg").defaultTo(0);
      table.bigInteger("idapp").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table.bigInteger("idsubtenant").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_orguserassign", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("userid").defaultTo(0);
      table.bigInteger("orgid").defaultTo(0);
      table.bigInteger("grpid").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_owner", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("ownname").notNullable();
      table.integer("ownstatus").defaultTo(0);
      table.integer("owntype").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_tenant_user_groupacl", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("iduser").notNullable();
      table.bigInteger("idgroupuseracl").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table.bigInteger("idapplication").defaultTo(0);
      table.bigInteger("created_byid").defaultTo(0);
      table.integer("grouptype").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_tenant_user", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("fullname").notNullable();
      table.string("userid").notNullable();
      table.string("pwd").notNullable();
      table.bigInteger("creator_stat").defaultTo(0);
      table.bigInteger("creator_byid").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table.bigInteger("idsubtenant").defaultTo(0);
      table.integer("leveltenant").defaultTo(0);
      table.integer("active").defaultTo(0);
      table.integer("total_attempt").defaultTo(0);
      table.integer("type_user").defaultTo(0);
      table.timestamp("locked_at", { useTz: false });
      table.timestamp("last_login", { useTz: false });
      table.timestamp("last_change_password", { useTz: false });
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cdid_tenant", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("tnname").notNullable();
      table.integer("tnstatus").defaultTo(0);
      table.integer("tntype").defaultTo(0);
      table.bigInteger("cdidowner").notNullable();
      table.integer("tnflag").defaultTo(1);
      table.integer("tnparentid").defaultTo(0);
      table
        .string("cdtenant")
        .defaultTo("", (options = { constraintName: (string = undefined) }));
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("cicd_menuuseracl", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("iduser").notNullable();
      table.integer("idmodule").defaultTo(0);
      table.integer("idmenu").defaultTo(0);
      table.bigInteger("idgroupuser").notNullable();
      table.integer("stat").defaultTo(1);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("event_log", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("request_ip").notNullable();
      table.string("request_url").notNullable();
      table.string("request_method").notNullable();
      table.string("request_service").notNullable();
      table.timestamp("request_date", { useTz: false });
      table.text("response_data").defaultTo(1);
      table.timestamp("response_date", { useTz: false });
      table.text("request_token");
      table.text("request_body");
      table.text("request_header");
      table.bigInteger("app_id").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_application", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("appname");
      table.string("applabel");
      table.string("description");
      table.integer("apptype").defaultTo(0);
      table.string("routelink");
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_biodata_corell", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("bioname");
      table.string("bioemailactive");
      table.string("biophoneactive");
      table.string("bioaddress");
      table.integer("bioidcorel").defaultTo(0);
      table.string("bionik");
      table.string("bionpwp");
      table.integer("biocorelobjid").defaultTo(0);
      table.integer("bioidtipenik").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_bussines_param", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("category");
      table.string("label");
      table.string("valuestr");
      table.integer("valueint").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_menu", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("name");
      table.string("title");
      table.string("routelink");
      table.string("routepath");
      table.integer("iconcode").defaultTo(0);
      table.integer("defaultid").defaultTo(0);
      table.string("created_byid").defaultTo(0);
      table.bigInteger("idmodule").defaultTo(0);
      table.string("description");
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_module", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("modulename");
      table.integer("created_byid").defaultTo(0);
      table.integer("status").defaultTo(1);
      table.integer("idapplication").defaultTo(0);
      table.string("modulecode");
      table.integer("moduletype").defaultTo(1);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_moduleby_tenant", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("modulename");
      table.integer("created_byid").defaultTo(0);
      table.integer("status").defaultTo(1);
      table.integer("idapplication").defaultTo(0);
      table.integer("idowner").defaultTo(0);
      table.integer("idtenant").defaultTo(0);
      table.string("modulecode");
      table.integer("moduletype").defaultTo(1);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_organizations", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("orgname");
      table.string("orgdescription");
      table.bigInteger("idowner").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table.integer("idsubtenant").defaultTo(0);
      table.integer("orglevel").defaultTo(0);
      table.bigInteger("created_byid").defaultTo(1);
      table.string("orgcode");
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_tenantgroup", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("groupname");
      table.bigInteger("idtenant").defaultTo(0);
      table.bigInteger("created_byid").defaultTo(1);
      table.integer("issubgroup").defaultTo(0);
      table.bigInteger("idapplication").defaultTo(0);
      table.bigInteger("idowner").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_usergroup", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("groupname");
      table.bigInteger("idtenant").defaultTo(0);
      table.integer("active").defaultTo(1);
      table.string("groupcode");
      table.integer("idapplication").defaultTo(0);
      table.integer("grouptype").defaultTo(1);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("mst_usergroupacl", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("idgroup").defaultTo(0);
      table.bigInteger("idtenant").defaultTo(0);
      table.bigInteger("idmodule").defaultTo(0);
      table.bigInteger("idmenu").defaultTo(0);
      table.integer("fcreate").defaultTo(0);
      table.integer("fread").defaultTo(0);
      table.integer("fupdate").defaultTo(0);
      table.integer("fdelete").defaultTo(0);
      table.integer("fview").defaultTo(0);
      table.integer("fapproval").defaultTo(0);
      table.integer("created_byid").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("pass_hist", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("user_id");
      table.string("pwd");
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("system_param", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("param_name");
      table.string("param_value");
      table.string("category");
      table.text("description");
      table.integer("idtenant").defaultTo(0);
      table.integer("idapp").defaultTo(0);
      table.integer("typeparam").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("trx_eventlog", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.bigInteger("created_by").defaultTo(0);
      table.text("valuejson");
      table.string("value");
      table.text("idapp");
      table.bigInteger("idmenu").defaultTo(0);
      table.string("description");
      table.bigInteger("cdaction").defaultTo(0);
      table.string("refevent");
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    })
    .createTable("trx_sessionlog", (table) => {
      table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.text("userinfo");
      table.timestamp("expire_date", { useTz: false });
      table.integer("created_by").defaultTo(0);
      table.integer("status").defaultTo(0);
      table.timestamp("created_at", { useTz: false }).defaultTo(knex.fn.now());
      table.timestamp("updated_at", { useTz: false }).defaultTo(knex.fn.now());
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable("cdid_adminnotification")
    .dropTable("cdid_dbcorellation")
    .dropTable("cdid_liscenses")
    .dropTable("cdid_orgapplications")
    .dropTable("cdid_orguserassign")
    .dropTable("cdid_owner")
    .dropTable("cdid_tenant_user_groupacl")
    .dropTable("cdid_tenant_user")
    .dropTable("cdid_tenant")
    .dropTable("cicd_menuuseracl")
    .dropTable("event_log")
    .dropTable("mst_application")
    .dropTable("mst_biodata_corell")
    .dropTable("mst_bussines_param")
    .dropTable("mst_menu")
    .dropTable("mst_module")
    .dropTable("mst_moduleby_tenant")
    .dropTable("mst_organizations")
    .dropTable("mst_tenantgroup")
    .dropTable("mst_usergroup")
    .dropTable("mst_usergroupacl")
    .dropTable("pass_hist")
    .dropTable("system_param")
    .dropTable("trx_eventlog")
    .dropTable("trx_sessionlog");
};
