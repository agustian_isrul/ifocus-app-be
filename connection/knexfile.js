// Update with your config settings.

module.exports = {
  // development: {
  //   client: "oracledb",
  //   connection: {
  //     user: "KKTORA",
  //     password: "P@ssw0rd.1", // mypw contains the hr schema password
  //     connectString: "182.169.41.225:1521/XE",
  //     database: "KKTORA",
  //   },
  //   migrations: {
  //     tableName: "kktadmmigra",
  //   },
  // },
  // development: {
  //   client: "pg",
  //   connection: {
  //     host: "10.11.100.116",
  //     database: "kktkomiportal",
  //     user: "postgres",
  //     password: "postgres",
  //   },
  //   pool: {
  //     min: 2,
  //     max: 10,
  //   },
  // },
  development: {
    client: "pg",
    connection: {
      host: "localhost",
      database: "kktkomiportaldb",
      user: "postgres",
      password: "postgres",
    },
    pool: {
      min: 2,
      max: 10,
    },
  },
};
