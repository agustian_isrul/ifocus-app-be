const Pool = require("pg").Pool;
const Client = require("pg").Client;

// const pool = new Pool({
//   host: "localhost",
//   user: "postgres",
//   password: "P@ssw0rd.1",
//   database: "kktkomidb",
//   port: 5432,
//   max: 10,
//   connectionTimeoutMillis: 0,
//   idleTimeoutMillis: 0,
// });

const pool = new Pool({
  host: "182.169.41.153",
  user: "postgres",
  password: "P@ssw0rd.1",
  database: "kktkomidbdev",
  port: 5432,
  max: 10,
  connectionTimeoutMillis: 0,
  idleTimeoutMillis: 0,
});

module.exports = pool;
