const HttpError = require("../routes/models/http-error");
const axios = require("axios");

const knex = require("../connection/dborm");
//let conn = knex.oracleKKTORA();

var conf = require("../config.json");
const apiserver = conf.kktserver;

const logs = require("../routes/services/utils/loger");
const fileLog = logs.logging();

Object.defineProperty(String.prototype, "capitalize", {
  value: function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
  },
  enumerable: false,
});

const getMenuFromPath = (routes) => {
  let menu = {
    bic: "Bank Participant",
    proxy: "Proxy Management",
    Proxy: "Proxy Management",
    sysparam: "System Param",
    channeltype: "Channel Type",
    prefund: "Prefund Management",
    trxcost: "Transaction Cost",
    branch: "Branch",
    limit: "Limit",
    idtype: "Identity Type",
    accounttype: "Account Type",
    resident: "Resident Type",
    custtype: "Customer Type",
    prefunddashboard: "Prefund Management",
    transaction: "Transaction Monitoring",
    proxytype: "Proxy Type",
    montrx: "Transaction Monitoring",
    reports: "Reports",
    log: "Log",
  };

  let route = routes + "";

  route = route.split("/");

  let result = menu[route[2]] ? menu[route[2]] : route;
  console.log(route[3]);
  console.log(result == "Log");
  result =
    result == "Log"
      ? route[3] == "getInBoundlog"
        ? "Inbound Log"
        : route[3] == "getOutBoundlog"
        ? "Outbond Log"
        : "System Log"
      : result;

  return result;
};

const getEventValue = (fullPath, menu, req) => {
  let eventVal =
    fullPath.includes("getAll") || fullPath.includes("get")
      ? `${menu} View  data`
      : fullPath.includes("get") && req.params
      ? `${menu} View Data With ${req.params}`
      : fullPath.includes("insert")
      ? `${menu} Insert New Data `
      : fullPath.includes("update")
      ? `${menu} Update Data `
      : fullPath.includes("delete")
      ? `${menu} Delete Data`
      : "";
  return eventVal;
};

const getDescription = (fullPath, routes, menu) => {
  let description =
    fullPath.includes("getAll") || fullPath.includes("get")
      ? `${routes[1].capitalize()} View  ${menu} data `
      : fullPath.includes("get") && req.params
      ? `${menu} View Data With ${req.params}`
      : fullPath.includes("insert")
      ? `${routes[1].capitalize()} Insert ${menu} New Data `
      : fullPath.includes("update")
      ? `${routes[1].capitalize()} Update ${menu} Data `
      : fullPath.includes("delete")
      ? `${routes[1].capitalize()} Delete ${menu} Data`
      : "";
  return description;
};

const getAction = (fullPath) => {
  let cdaction = fullPath.includes("getAll")
    ? 4
    : fullPath.includes("get")
    ? 4
    : fullPath.includes("insert")
    ? 5
    : fullPath.includes("update")
    ? 6
    : fullPath.includes("delete")
    ? 7
    : 4;
  return cdaction;
};

const insertIntoLogEvent = async (req, token) => {
  try {
    fileLog.info(`New request with token ${token} `);
    let fullPath = req.baseUrl + req.path;
    let routes = req.baseUrl.split("/");
    let menu = await getMenuFromPath(fullPath);
    let eventVal = await getEventValue(fullPath, menu, req);
    let idapp = req.userData.apps[0].idapp;
    let description = await getDescription(fullPath, routes, menu);
    let cdaction = await getAction(fullPath);

    //const token = req.headers.authorization;
    axios
      .post(
        apiserver + "/api/log/insertLog",
        {
          created_by: req.userData.id ? req.userData.id : 0,
          value: eventVal ? eventVal : 0,
          idapp: idapp ? idapp : 0,
          idmenu: 0,
          description: description ? description : 0,
          cdaction: cdaction ? cdaction : 0,
          refevent: req.tokenID ? req.tokenID : 0,
          valuejson: req.body ? JSON.stringify(req.body) : null,
        },
        {
          headers: { Authorization: token },
        }
      )
      .then(async (resp) => {
        return;
      })
      .catch((err) => {
        return;
      });

    fileLog.info(`Saving event to eventlog `);
    fileLog.info(`Request Event : ${description} `);
  } catch (err) {
    console.log(err);
  }
};

module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization; // Authorization: 'Bearer TOKEN'

    if (!token) {
      throw new Error("Authentication testing failed!");
    }
    let tok = token.split(" ");

    axios
      .get(apiserver + "/adm/auth/who", {
        headers: { Authorization: token },
      })
      .then(async (resp) => {
        // console.log(resp);
        if (resp.data) {
          req.userData = resp.data.data;
          req.tokenID = resp.data.tokenId;
          req.exp = resp.data.exp;
          await insertIntoLogEvent(req, token);

          next();
        } else {
          const error = new HttpError("Authentication failed!", 403);
          return next(error);
        }
      })
      .catch((err) => {
        console.log(">>>>>>>>>>>>>>>>>> " + err);
        const error = new HttpError("Authentication 403 failed!", 403);
        return next(error);
      });
  } catch (err) {
    const error = new HttpError("Authentication failed!", 403);
    return next(error);
  }
};
